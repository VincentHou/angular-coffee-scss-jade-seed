(function() {
  var tests;

  tests = Object.keys(window.__karma__.files).filter(function(file) {
    return /Spec.js$/.test(file);
  });

  requirejs.config({
    deps: tests,
    baseUrl: '/base/scripts',
    paths: {
      'jQuery': '../vendor/jquery/jquery',
      'angular': '../vendor/angular/angular',
      'angular-resource': '../vendor/angular-resource/angular-resource',
      'angularMocks': '../vendor/angular-mocks/angular-mocks'
    },
    shim: {
      'angular': {
        'exports': 'angular'
      },
      'angular-resource': {
        deps: ['angular']
      },
      'angularMocks': {
        exports: 'angular.mock',
        deps: ['angular']
      },
      'jQuery': {
        'exports': 'jQuery'
      }
    },
    callback: window.__karma__.start
  });

}).call(this);
