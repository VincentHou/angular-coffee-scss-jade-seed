"use strict"
lrSnippet = require("grunt-contrib-livereload/lib/utils").livereloadSnippet
mountFolder = (connect, dir) ->
  connect.static require("path").resolve(dir)

module.exports = (grunt) ->

  # load all grunt tasks
  require("matchdep").filterDev("grunt-*").forEach grunt.loadNpmTasks

  # configurable paths
  APP_DIR = "app"
  VENDOR_DIR = "app/vendor"
  BUILD_DIR = "build"
  DIST_DIR = "dist"

  grunt.initConfig

    watch:
      options:
        livereload: true
        nospawn: true
      coffee:
        files: ["<%= APP_DIR %>/scripts/**/{,*/}*.coffee"]
        tasks: ["coffee:build"]
        options:
          events: ['changed', 'added']
      coffeeTest:
        files: ["test/spec/{,*/}*.coffee"]
        tasks: ["coffee:test", "karma:unit:run"]
      compass:
        files: ["<%= APP_DIR %>/styles/{,*/}*.{scss,sass}"]
        tasks: ["compass"]

    connect:
      options:
        port: 9000
        # Change this to '0.0.0.0' to access the server from outside.
        hostname: "localhost"
      livereload:
        options:
          middleware: (connect) ->
            [lrSnippet, mountFolder(connect, yeomanConfig.build), mountFolder(connect, yeomanConfig.app)]
      production:
        options:
          middleware: (connect) ->
            [mountFolder(connect, yeomanConfig.dist)]

    open:
      server:
        url: "http://<%= connect.options.hostname %>:<%= connect.options.port %>"

    clean:
      dist: "<%= DIST_DIR %>"
      build: "<%= BUILD_DIR %>"

    coffee:
      # options:
      #   sourceMap: yes
      #   sourceRoot: './'
      build:
        expand: true
        cwd: "<%= APP_DIR %>/scripts"
        src: "**/{,*/}*.coffee"
        dest: "<%= BUILD_DIR %>/scripts"
        ext: ".js"
      test:
        expand: true
        cwd: "test"
        src: "**/{,*/}*.coffee"
        dest: "<%= BUILD_DIR %>"
        ext: ".js"

    compass:
      options:
        sassDir: "<%= APP_DIR %>/styles"
        cssDir: "<%= BUILD_DIR %>/styles"
        # imagesDir: "<%= APP_DIR %>/images"
        # javascriptsDir: "<%= APP_DIR %>/scripts"
        # fontsDir: "<%= APP_DIR %>/styles/fonts"
        # importPath: "<%= VENDOR_DIR %>/foundation/scss"
        relativeAssets: true
      dev:
        options:
          debugInfo: true

    jade:
      options:
        pretty: true
      build:
        expand: true
        cwd: "<%= APP_DIR %>/views"
        src: "**/{,*/}*.jade"
        dest: "<%= BUILD_DIR %>/views"
        ext: ".html"

    imagemin:
      dist:
        files: [
          expand: true
          cwd: "<%= APP_DIR %>/images"
          src: "{,*/}*.{png,jpg,jpeg}"
          dest: "<%= DIST_DIR %>/images"
        ]

    cssmin:
      dist:
        files:
          "<%= DIST_DIR %>/styles/main.css": [
            "<%= BUILD_DIR %>/styles/{,*/}*.css",
            "<%= APP_DIR %>/styles/{,*/}*.css"
          ]

    htmlmin:
      dist:
        removeCommentsFromCDATA: true
        # https://github.com/yeoman/grunt-usemin/issues/44
        collapseWhitespace: true
        collapseBooleanAttributes: true
        removeAttributeQuotes: true
        removeRedundantAttributes: true
        useShortDoctype: true
        removeEmptyAttributes: true
        removeOptionalTags: true
        files: [
          expand: true
          cwd: "<%= APP_DIR %>"
          src: ["*.html", "views/**/*.html"]
          dest: "<%= DIST_DIR %>"
        ]

    ngmin:
      dist:
        files: [
          expand: true,
          src: ["<%= BUILD_DIR %>/scripts/**/*.js"]
        ]

    requirejs:
      compile:
        options:
          name: "main"
          baseUrl: "<%= BUILD_DIR %>/scripts"
          mainConfigFile: "<%= BUILD_DIR %>/scripts/main.js"
          out: "<%= DIST_DIR %>/scripts/main.js"

    useminPrepare:
      html: "<%= BUILD_DIR %>/index.html"
      dest: "<%= DIST_DIR %>"

    usemin:
      html: ["<%= DIST_DIR %>/{,*/}*.html"]
      css: ["<%= DIST_DIR %>/styles/{,*/}*.{/}*.css"]

    rev:
      dist:
        files:
          src: [
            "<%= DIST_DIR %>/scripts/{,*/}*.js"
            "<%= DIST_DIR %>/styles/{,*/}*.css"
            "<%= DIST_DIR %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}"
            "<%= DIST_DIR %>/styles/fonts/*"
          ]

    copy:
      build:
        files: [
            expand: true, dot: false, cwd: "<%= APP_DIR %>", dest: "<%= BUILD_DIR %>",
            src: [
              "index.html", "vendor/pure/**/*.css",
              "vendor/*/*.js", "vendor/json3/lib/*.js",
              "!vendor/**/*.min.js", "!vendor/**/Gruntfile.js"
            ]
        ]
      test:
        files: [
          expand: true, dot: false, cwd: "<%= APP_DIR %>", dest: "<%= BUILD_DIR %>",
          src: [
            "vendor/*/*.js", "vendor/json3/lib/*.js",
            "!vendor/**/*.min.js", "!vendor/**/Gruntfile.js",
          ]
        ]
      dist:
        files: [
            expand: true, dot: false, cwd: "<%= APP_DIR %>", dest: "<%= DIST_DIR %>",
            src: ["*.{ico,txt}", "images/{,*/}*.{gif,webp}", "styles/fonts/*", "vendor/requirejs/require.js"]
        ]

    karma:
      options:
        basePath: "<%= BUILD_DIR %>"
        configFile: "karma.conf.js"
      unit:
        background: true
        browsers: ['Chrome']
      ci:
        singleRun: true
        browsers: ["PhantomJS"]


  # just recreate changed coffee file
  changedFiles = Object.create(null);
  onChange = grunt.util._.debounce(->
    grunt.config 'coffee.build.src', Object.keys(changedFiles)
    changedFiles = Object.create(null);
  , 200);
  grunt.event.on 'watch', (action, filepath, target) ->
    if grunt.file.isMatch grunt.config('watch.coffee.files'), filepath
      filepath = filepath.replace( grunt.config('coffee.build.cwd')+'/', '' );
      changedFiles[filepath] = action;
      onChange();


  # compile coffeescript, scss
  grunt.registerTask "compile", ["coffee", "compass", "jade"]
  # build all stuff for development and ready for production
  grunt.registerTask "build", ["clean:build", "compile"]
  # build production stuff
  grunt.registerTask "min", ["useminPrepare", "imagemin", "concat", "cssmin", "htmlmin", "ngmin", "requirejs", "rev", "usemin"]

  # for travis, CI testing
  grunt.registerTask "ci", ["build", "copy:test", "karma:ci"]
  # run testing while there is any things be changed
  grunt.registerTask "autotest", ["karma:unit", "watch"]

  # setup development env, autocompile, livereload and open the browers.
  grunt.registerTask "dev", ["build", "connect:livereload", "open", "watch"]
  # simulate production env.
  grunt.registerTask "dist", ["clean:dist", "build", "copy:build", "min", "copy:dist", "connect:production", "open", "watch"]

  grunt.registerTask "default", ["dev"]
